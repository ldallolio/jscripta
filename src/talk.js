const { NlpManager, ConversationContext } = require('node-nlp');
const readline = require('readline');
const Handlebars = require('handlebars');
const threshold = 0.5;

async function executeAction(source, name, parameters, context) {
  return new Promise(resolve => {
    const params = JSON.parse(`[${parameters}]`);
    var evalParams = params.map(param => Handlebars.compile(param)(context));
    if (source.actions[name]) {
      const action = source.actions[name](source, context, ...(evalParams || []));
      if (action.then) {
        action.then(() => resolve());
      } else {
        return resolve();
      }
    }
    return resolve();
  });
}

async function talk() {
  try {
    const manager = new NlpManager({ languages: ['en'], ner: { builtins: [] } });
    const context = new ConversationContext();
    context.heap = {};
    manager.load("./model/scripta.nlp");
    manager.actions = {}
    manager.actions.setVariable = (source, context, container, expr) => {
      context.heap[container] = expr;
      return Promise.resolve(container);
    };
    manager.actions.getVariable = (source, context, container) => {
      context.it = context.heap[container];
      return Promise.resolve(container);
    };
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
      terminal: false,
    });
    rl.on('line', async line => {
      if (line.toLowerCase() === 'quit') {
        rl.close();
        process.exit();
      } else {
        const result = await manager.process('en', line, context);
        var answer; 
        if (result.score > threshold) {
          const promises = result.actions.map(action =>
            executeAction(manager, action.action, action.parameters, context)
          );
          //console.log(context);
          answer = Handlebars.compile(result.srcAnswer)(context);
          await Promise.all(promises);
        } else {
          answer = "Sorry, I don't understand";;
        }
        console.log(`bot> ${answer}`);
        process.stdout.write("user>");
      }
    });
    process.stdout.write("user>");
  } catch (error) {
    console.log(error);
  }
}

talk();