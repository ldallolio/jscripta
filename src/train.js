const { NlpManager, NlpExcelReader } = require('node-nlp');

async function train() {
    try {
        const manager = new NlpManager({ languages: ['en'], ner: { builtins: [] } });
        
        //const reader = new NlpExcelReader(manager);
        //reader.load('./data/rules.xls');
        // https://github.com/axa-group/nlp.js#readme
        // http://www.jaedworks.com/hypercard/scripts/hypertalk-bnf.html

        const putExprEntity = manager.addTrimEntity('putExpr');
        putExprEntity.addBetweenCondition('en', 'put', 'into');
        putExprEntity.addAfterCondition('en', 'put');
        const putContainerEntity = manager.addTrimEntity('putContainer');
        putContainerEntity.addAfterLastCondition('en', 'into'); 
        manager.addDocument('en', 'put %putExpr% into %putContainer%', 'put');
        manager.slotManager.addSlot('put', 'putExpr', true, { en: 'What do you want to insert into {{putContainer}}?' });
        manager.slotManager.addSlot('put', 'putContainer', true, { en: 'Where do you want to put {{putExpr}}?' });
        manager.addAnswer('en', 'put', '{{putContainer}} has been set to {{putExpr}}');
        manager.addAction('put', 'setVariable', '"{{putContainer}}","{{putExpr}}"');
        manager.assignDomain('en', 'put', 'commands');

        const getContainerEntity = manager.addTrimEntity('getContainer');
        getContainerEntity.addAfterCondition('en', 'get'); 
        manager.addDocument('en', 'get %getContainer%', 'get');
        manager.slotManager.addSlot('get', 'getContainer', true, { en: 'Please specify the name to inspect ?' });
        manager.addAnswer('en', 'get', '{{getContainer}} is {{it}}');
        manager.addAction('get', 'getVariable', '"{{getContainer}}"');
        manager.assignDomain('en', 'get', 'commands');        

        const divideFloatEntity = manager.addTrimEntity('divideFloat');
        divideFloatEntity.addAfterLastCondition('en', 'divide'); 
        const divideContainerEntity = manager.addTrimEntity('divideContainer');
        divideContainerEntity.addBetweenCondition('en', 'divide', 'by');
        divideContainerEntity.addAfterCondition('en', 'divide');
        manager.addDocument('en', 'divide %divideContainer% by %divideFloat%', 'divide');
        manager.slotManager.addSlot('divide', 'divideFloat', true, { en: 'By which value do you want to divide?' });
        manager.slotManager.addSlot('divide', 'divideContainer', true, { en: 'What do you want to divide?' });
        manager.addAnswer('en', 'divide', '{{divideContainer}} has been divided by {{divideFloat}}');
        manager.addAction('divide', 'div', '{{divideContainer}},{{divideFloat}}');
        manager.assignDomain('en', 'divide', 'commands');

        await manager.train();        
        manager.save("./model/scripta.nlp"); 
        console.log("Training completed.");    
        /*manager
        .process(' put 76.55 into var1')
        .then(result => console.log(result))
        .catch(err => console.log(err));*/           
    } catch (error) {
        console.log(error);
    }

}

train();